﻿using DeliverySystem.Logic;
using DeliverySystem.Model;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
namespace DeliverySystem.UI
{
    /// <summary>
    /// Interaction logic for ReceiverList.xaml
    /// </summary>
    public partial class ReceiverList : Window
    {
        private ObservableCollection<Receiver> list;
        public ReceiverList()
        {
            InitializeComponent();
            list = DBFactory.getPersonDB().GetReceivers();
            this.dgList.ItemsSource = list;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Receiver r = dgList.SelectedItem as Receiver;
            ReceiverWindow rw = new ReceiverWindow(r);
            rw.ShowDialog();
        }

        private void tbLostFocus(object sender, RoutedEventArgs e)
        {
            var snd = sender as TextBox;
            if (snd == null) return;
            if (string.IsNullOrEmpty(snd.Text.Trim())) { dgList.ItemsSource = list; return; }
            var list2 = list.Where(x => x.ID.Equals(snd.Text.Trim())).ToList();
            if (list2.Count != 0) dgList.ItemsSource = list2;
        }
    }
}
