﻿using DeliverySystem.Logic;
using DeliverySystem.Model;
using System.Collections.ObjectModel;
using System.Windows;

namespace DeliverySystem.UI
{
    /// <summary>
    /// Interaction logic for PersonWindow.xaml
    /// </summary>
    public partial class ReceiverWindow : Window
    {
        Receiver p;
        ObservableCollection<Package> list = new ObservableCollection<Package>();
        bool isNew = false;
        public ReceiverWindow()
        {
            InitializeComponent();
        }
        
        public ReceiverWindow(Receiver r)
        {
            InitializeComponent();
            p = r;
            this.DataContext = p;
            list = PackageFactory.GetPackageByPerson(p);
            dgList.ItemsSource = list;
            SetPrice();
        }
        private void SetPrice()
        {
            decimal temp = 0;
            foreach (var pack in list)
                temp += pack.PVP;
            tbTotalPrice.Text = temp.ToString();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            p = PersonFactory.GetReceiver(tbID.Text.Trim());
            if(p == null)
            {
                p = new Receiver();
                isNew = true;
            }
            this.DataContext = p;
            list = PackageFactory.GetPackageByPerson(p);
            dgList.ItemsSource = list;
        }

        private void dgList_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            Package pack = dgList.SelectedItem as Package;
            tbSenderCity.Text = pack.Receiver.City;
            tbSenderName.Text = pack.Receiver.Name;
            tbSenderSurname.Text = pack.Receiver.Surname;
        }
    }
}
