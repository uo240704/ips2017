﻿using DeliverySystem.Logic;
using DeliverySystem.Model;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
namespace DeliverySystem.UI
{
    /// <summary>
    /// Interaction logic for PackageList.xaml
    /// </summary>
    public partial class PackageList : Window
    {
        ObservableCollection<Package> packages = new ObservableCollection<Package>();
        public PackageList()
        {
            InitializeComponent();
            packages = DBFactory.getPackageDB().getPackages();
            dgList.ItemsSource = packages;
        }
        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            Package p = dgList.SelectedItem as Package;
            PackageWindow pw = new PackageWindow(p);
            pw.ShowDialog();
        }

        private void tbLostFocus(object sender, RoutedEventArgs e)
        {
            var snd = sender as TextBox;
            if (snd == null) return;
            if(string.IsNullOrEmpty(snd.Text.Trim())) { dgList.ItemsSource = packages; return; }
            var list2 = packages.Where(x => x.ID.Equals(snd.Text.Trim()));
            dgList.ItemsSource = list2;
        }
    }
}
