﻿using DeliverySystem.Logic;
using DeliverySystem.Model;
using System.Windows;

namespace DeliverySystem.UI
{
    /// <summary>
    /// Interaction logic for PackageWindow.xaml
    /// </summary>
    /// 
    public partial class PackageWindow : Window
    {
        Package pack;
        public PackageWindow()
        {
            InitializeComponent();
        }
        public PackageWindow(Package p)
        {
            InitializeComponent();
            pack = p;
            this.DataContext = pack;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            pack = PackageFactory.GetPackage(tbID.Text);
            this.DataContext = pack;
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SenderWindow sw = new SenderWindow(pack.Sender);
            sw.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            ReceiverWindow rw = new ReceiverWindow(pack.Receiver);
            rw.ShowDialog();
        }
    }
}
