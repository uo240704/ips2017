﻿using DeliverySystem.Logic;
using DeliverySystem.Model;
using System.Collections.ObjectModel;
using System.Windows;
using System.Windows.Controls;
using System.Linq;
namespace DeliverySystem.UI
{
    /// <summary>
    /// Interaction logic for SenderList.xaml
    /// </summary>
    public partial class SenderList : Window
    {
        private ObservableCollection<Sender> list;
        public SenderList()
        {
            InitializeComponent();
            list = DBFactory.getPersonDB().GetSenders();
            dgList.ItemsSource = list;

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Sender s = dgList.SelectedItem as Sender;
            SenderWindow sw = new SenderWindow(s);
            sw.ShowDialog();
        }

        private void tbLostFocus(object sender, RoutedEventArgs e)
        {
            var snd = sender as TextBox;
            if (snd == null) return;
            if (string.IsNullOrEmpty(snd.Text.Trim())) { dgList.ItemsSource = list; return; }
            var list2 = list.Where(x => x.ID.Equals(snd.Text.Trim())).ToList();
            if (list2.Count != 0) dgList.ItemsSource = list2;
        }
    }
}
