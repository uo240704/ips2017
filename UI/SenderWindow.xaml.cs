﻿using DeliverySystem.Logic;
using DeliverySystem.Model;
using System.Collections.ObjectModel;
using System.Windows;

namespace DeliverySystem.UI
{
    /// <summary>
    /// Interaction logic for PersonWindow.xaml
    /// </summary>
    public partial class SenderWindow : Window
    {
        Sender p;
        ObservableCollection<Package> list = new ObservableCollection<Package>();
        bool isNew = false;
        public SenderWindow()
        {
            InitializeComponent();
        }

        public SenderWindow(Sender s)
        {
            InitializeComponent();
            p = s;
            this.DataContext = p;
            list = PackageFactory.GetPackageByPerson(p);
            dgList.ItemsSource = list;
            SetPrice();
        }
        private void SetPrice()
        {
            decimal temp = 0;
            foreach (var pack in list)
                temp += pack.PVP;
            tbTotalPrice.Text = temp.ToString();
        }
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            Package pac = new Package
            {
                Sender = p,
                Weight = decimal.Parse(tbPackageWeight.Text),
                Discount = decimal.Parse(tbPackageDiscount.Text),
                Description = tbPackageDescription.Text,
                Receiver = PersonFactory.GetReceiver(tbReceiverID.Text),
                CurrentLocation = "Oviedo",
                TransportID = PackageFactory.AssignTransport(decimal.Parse(tbPackageWeight.Text))
                
            };
            pac.SetPrice();
            list.Add(pac);
            pac.Sender.SentPackages.Add(pac);
            pac.Receiver.PendingPackages.Add(pac);
            DBFactory.getPackageDB().add(pac);
            SetPrice();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {
            p = PersonFactory.GetPerson(tbID.Text.Trim());
            if(p == null)
            {
                p = new Sender();
                isNew = true;
            }
            this.DataContext = p;
            list = PackageFactory.GetPackageByPerson(p);
            dgList.ItemsSource = list;
            SetPrice();
        }

        private void Guardar_click(object sender, RoutedEventArgs e)
        {

        }

        private void Baja_click(object sender, RoutedEventArgs e)
        {

        }
    }
}
