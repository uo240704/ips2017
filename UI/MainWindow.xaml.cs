﻿using DeliverySystem.Logic;
using DeliverySystem.UI;
using System.Windows;
namespace DeliverySystem
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
            DBFactory.getPersonDB();
            DBFactory.getPackageDB();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            SenderList send = new SenderList();
            send.ShowDialog();
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            PackageList pack = new PackageList();
            pack.ShowDialog();
        }

        private void Button_Click_2(object sender, RoutedEventArgs e)
        {
            SendPackage sp = new SendPackage();
            sp.ShowDialog();
        }

        private void Button_Click_3(object sender, RoutedEventArgs e)
        {
            ReceiverList receive = new ReceiverList();
            receive.ShowDialog();
        }
    }
}
