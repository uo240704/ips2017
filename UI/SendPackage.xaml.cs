﻿using DeliverySystem.DB;
using DeliverySystem.Logic;
using DeliverySystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace DeliverySystem.UI
{
    /// <summary>
    /// Interaction logic for SendPackage.xaml
    /// </summary>
    public partial class SendPackage : Window
    {
        Package p;
        Sender s;
        Receiver r;
        public SendPackage()
        {
            InitializeComponent();
            p = new Package();
            s = new Sender();
            r = new Receiver();
            p.Sender = s;
            p.Receiver = r;
        }

        private void TextBox_LostFocus(object sender, RoutedEventArgs e)
        {

        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            if (PersonFactory.GetPerson(p.Sender.ID) == null)
            {
                p.Receiver.PendingPackages = new List<Package>();
                p.Receiver.PendingPackages.Add(p);
                DBFactory.getPersonDB().AddReceiver(p.Receiver);
            }
            else
                p.Receiver.PendingPackages.Add(p);

            if (PersonFactory.GetReceiver(p.Receiver.ID) == null)
            {
                p.Sender.SentPackages = new List<Package>();
                p.Sender.SentPackages.Add(p);
                DBFactory.getPersonDB().AddSender(p.Sender);
            }
            else
                p.Sender.SentPackages.Add(p);

            PackageFactory.createPackage(p.Weight, p.PVP, p.Discount, p.Description, p.Sender.ID, p.Receiver.ID, p.CurrentLocation, p.TransportID);
            Close();
        }

        private void TextBox_LostFocus_1(object sender, RoutedEventArgs e)
        {
            var snd = sender as TextBox;
            if (snd == null) return;
            var id = snd.Text.Trim();
            var Sender = PersonFactory.GetPerson(id);
            if (Sender != null) { p.Sender = Sender; this.DataContext = p; }
        }

        private void TextBox_LostFocus_2(object sender, RoutedEventArgs e)
        {
            var snd = sender as TextBox;
            if (snd == null) return;
            var id = snd.Text.Trim();
            var Receiver = PersonFactory.GetReceiver(id);
            if (Receiver != null) { p.Receiver = Receiver; this.DataContext = null; this.DataContext = p; }
        }
    }
}
