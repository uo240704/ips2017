﻿using DeliverySystem.Logic;
using DeliverySystem.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverySystem.DB
{
    class PackageDB
    {
        private ObservableCollection<Package> packages = new ObservableCollection<Package>();

        Random rand = new Random();
        public PackageDB()
        {
            for(int i = 0; i < 1000; i++)
            {
                Sender tempSender = PersonFactory.GetRandomSender();
                Receiver tempReceiver = PersonFactory.GetRandomReceiver();
                Package tempPack = new Package
                {
                    Weight = rand.Next(50),
                    Discount = rand.Next(10),
                    Description = "PlaceHolder",
                    Sender = tempSender,
                    Receiver = tempReceiver,
                    TransportID = "25",
                    CurrentLocation = "Oviedo"
                };
                tempPack.SetPrice();
                int randPart = (int)(tempPack.Weight * rand.Next(5) + tempPack.PVP * rand.Next(25) + rand.Next(95));
                tempPack.ID = randPart.ToString();
                packages.Add(tempPack);
                tempSender.SentPackages.Add(tempPack);
                tempReceiver.PendingPackages.Add(tempPack);
                DBFactory.getPersonDB().AddSender(tempSender);
                DBFactory.getPersonDB().AddReceiver(tempReceiver);
            }
        }
        public ObservableCollection<Package> getPackages()
        {
            return packages;
        }

        public void add(Package pack)
        {
            packages.Add(pack);
        }
    }
}
