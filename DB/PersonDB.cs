﻿using DeliverySystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;
using System.Collections.ObjectModel;

namespace DeliverySystem.DB
{
    class PersonDB
    {
        List<Person> people = new List<Person>();
        ObservableCollection<Sender> senders = new ObservableCollection<Sender>();
        ObservableCollection<Receiver> receivers = new ObservableCollection<Receiver>();

        private List<String> Names = new List<String>
        {
            "Andres",
            "Manuel",
            "Ivan",
            "Lara",
            "Maria",
            "Fernando",
            "Miguel",
            "Alejandro",
            "Cristina"
        };

        public ObservableCollection<Sender> GetSenders()
        {
            return senders;
        }

        public ObservableCollection<Receiver> GetReceivers()
        {
            return receivers;
        }

        public void AddSender(Sender send)
        {
            senders.Add(send);
        }

        public void AddReceiver(Receiver rec)
        {
            receivers.Add(rec);
        }

        private List<String> Surnames = new List<String>
        {
            "Gonzalez",
            "Fernandez",
            "Lopez",
            "Garcia",
            "Prieto",
            "Hidalgo",
            "Mendez",
            "Posada"
        };

        private List<String> Letters = new List<String>
        {
            "A",
            "B",
            "C",
            "D",
            "E",
            "F",
            "G",
            "H",
            "I",
            "J",
            "K",
            "L",
            "M",
            "N",
            "O",
            "P",
            "Q",
            "R",
            "T",
            "U",
            "V",
            "W",
            "X",
            "Y",
            "Z"
        };
        Random rand = new Random();
        public PersonDB()
        {
            for(int i = 0; i < 100; i++)
            {
                people.Add(new Person
                {
                    Name = Names[rand.Next(Names.Count)],
                    Surname = Surnames[rand.Next(Surnames.Count)] + " " + Surnames[rand.Next(Surnames.Count)],
                    ID = rand.Next(100000, 999999).ToString(),
                    City = "Oviedo",
                    Country = "Spain",
                    County = "Asturias",
                    PostalCode = rand.Next(33000, 33080).ToString(),
                    Street = "" + Letters[rand.Next(Letters.Count)] + Letters[rand.Next(Letters.Count)] + Letters[rand.Next(Letters.Count)] + Letters[rand.Next(Letters.Count)] + " Nr: " + rand.Next(100).ToString()
                });
            }
        }
        public List<Person> GetPeople()
        {
            return people;
        }
        public void add(Person pers)
        {
            people.Add(pers);
        }
        public Person getRandom()
        {
            return people[rand.Next(people.Count)];
        }
    }
}
