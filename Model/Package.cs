﻿namespace DeliverySystem.Model
{
    public class Package
    {
        public string ID { get; set; }
        public decimal Weight { get; set; }
        public decimal PVP { get; set; }
        public decimal Discount { get; set; }
        public string Description { get; set; }
        public Sender Sender { get; set; }
        public Receiver Receiver { get; set; }
        public string TransportID { get; set; }
        public string CurrentLocation { get; set; }

        public void SetPrice()
        {
            if (Weight < 10)
                PVP = Weight * new decimal(1.8);
            else if (Weight < 20)
                PVP = Weight * new decimal(1.5);
            else
                PVP = Weight * new decimal(1.2);
            if (Discount > 0)
                PVP = PVP * (1-(Discount / 100));
        }
    }
}
