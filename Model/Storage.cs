﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverySystem.Model
{
    class Storage
    {
        public string ID { get; set; }
        public string City { get; set; }
        public string Country { get; set; }
        public string County { get; set; }
        public string PostalCode { get; set; }
        public string Street { get; set; }
    }
}
