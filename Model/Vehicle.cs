﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverySystem.Model
{
    class Vehicle
    {
        public string ID { get; set; }
        public decimal Capacity { get; set; }
    }
}
