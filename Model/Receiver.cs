﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverySystem.Model
{
    public class Receiver : Person
    {
        public List<Package> PendingPackages { get; set; }
    }
}
