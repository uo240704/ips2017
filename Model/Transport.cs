﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverySystem.Model
{
    class Transport
    {
        public string ID { get; set; }
        public Vehicle Veh { get; set; }
        public List<Package> packages { get; set; }
    }
}
