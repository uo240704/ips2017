﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverySystem.Model
{
    public class Sender : Person
    {
        public List<Package> SentPackages { get; set; }
    }
}
