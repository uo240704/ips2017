﻿using DeliverySystem.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverySystem.Logic
{
    class PackageFactory
    {
        public static Package createPackage(decimal Weight, decimal PVP, decimal Discount, string Description, string Sender, string Receiver, string Location, string Transport)
        {
            Package pack = new Package
            {
                Sender = PersonFactory.GetPerson(Sender),
                Receiver = PersonFactory.GetReceiver(Receiver),
                ID = DateTime.Now.ToString("yyyyMMdd"),
                Weight = Weight,
                PVP = PVP,
                Discount = Discount,
                Description = Description,
                CurrentLocation = Location,
                TransportID = Transport
            };
            pack.SetPrice();
            DBFactory.getPackageDB().add(pack);
            return pack;
        }
        public static Package createPackage(decimal Weight, decimal PVP, decimal Discount, string Description, string Sender, string Receiver)
        {
            Package pack = new Package
            {
                Sender = PersonFactory.GetPerson(Sender),
                Receiver = PersonFactory.GetReceiver(Receiver),
                ID = DateTime.Now.ToString("yyyyMMdd"),
                Weight = Weight,
                PVP = PVP,
                Discount = Discount,
                Description = Description
            };
            pack.SetPrice();
            DBFactory.getPackageDB().add(pack);
            return pack;
        }

        public static Package GetPackage(string ID)
        {
            return DBFactory.getPackageDB().getPackages().Where(pack => pack.ID.Equals(ID)).ToArray<Package>()[0];
        }
        public static ObservableCollection<Package> GetPackageByPerson(Person person)
        {
            ObservableCollection<Package> list = new ObservableCollection<Package>();
            foreach (var pack in DBFactory.getPackageDB().getPackages().Where(package => package.Sender.ID.Equals(person.ID)))
                list.Add(pack);
            return list;
        }
        public static ObservableCollection<Package> GetPackageByReceiver(Person person)
        {
            ObservableCollection<Package> list = new ObservableCollection<Package>();
            foreach (var pack in DBFactory.getPackageDB().getPackages().Where(package => package.Receiver.ID.Equals(person.ID)))
                list.Add(pack);
            return list;
        }
        internal static string AssignTransport(decimal weight)
        {
            return "25";
        }
    }
}
