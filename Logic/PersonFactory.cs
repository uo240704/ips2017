﻿using DeliverySystem.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverySystem.Logic
{
    static class PersonFactory
    {

        public static Person CreatePerson(string Name, string Surname, string ID, string County, string Country, string City, string PostalCode, string Street)
        {
            Person pers = new Person
            {
                Name = Name,
                Surname = Surname,
                ID = ID,
                Country = Country,
                County = County,
                City = City,
                PostalCode = PostalCode,
                Street = Street
            };
            DBFactory.getPersonDB().add(pers);
            return pers;
        }

        public static Sender GetPerson(string ID)
        {
            var list = DBFactory.getPersonDB().GetPeople().Where(person => person.ID.Equals(ID)).ToArray<Person>();
            if (list.Length == 0) return null;
            Person temp = DBFactory.getPersonDB().GetPeople().Where(person => person.ID.Equals(ID)).ToArray<Person>()[0];
            Sender send = new Sender
            {
                ID = temp.ID,
                Name = temp.Name,
                Surname = temp.Surname,
                City = temp.City,
                Country = temp.Country,
                County = temp.County,
                PostalCode = temp.PostalCode,
                SentPackages = new List<Package>(),
                Street = temp.Street
            };
            send.SentPackages = PackageFactory.GetPackageByPerson(send).ToList<Package>();
        return send;
        }

        public static Receiver GetReceiver(string ID)
        {
            Person temp = DBFactory.getPersonDB().GetPeople().Where(person => person.ID.Equals(ID)).ToArray<Person>()[0];
            Receiver rec = new Receiver
            {
                ID = temp.ID,
                Name = temp.Name,
                Surname = temp.Surname,
                City = temp.City,
                Country = temp.Country,
                County = temp.County,
                PostalCode = temp.PostalCode,
                PendingPackages = new List<Package>(),
                Street = temp.Street
            };
            rec.PendingPackages = PackageFactory.GetPackageByReceiver(rec).ToList<Package>();
            return rec;
        }

        public static Receiver GetRandomReceiver()
        {
            Person temp = DBFactory.getPersonDB().getRandom();
            Receiver rec = new Receiver
            {
                ID = temp.ID,
                Name = temp.Name,
                Surname = temp.Surname,
                City = temp.City,
                Country = temp.Country,
                County = temp.County,
                PostalCode = temp.PostalCode,
                PendingPackages = new List<Package>(),
                Street = temp.Street
            };
            return rec;
        }

        public static Sender GetRandomSender()
        {
            Person temp = DBFactory.getPersonDB().getRandom();
            Sender rec = new Sender
            {
                ID = temp.ID,
                Name = temp.Name,
                Surname = temp.Surname,
                City = temp.City,
                Country = temp.Country,
                County = temp.County,
                PostalCode = temp.PostalCode,
                SentPackages = new List<Package>(),
                Street = temp.Street
            };
            return rec;
        }
    }
}
