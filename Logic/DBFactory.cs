﻿using DeliverySystem.DB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DeliverySystem.Logic
{
    static class DBFactory
    {
        private static PersonDB pers;
        private static PackageDB packages;
        private static OfficeDB offices;
        private static StorageDB storages;
        private static TransportDB transports;
        private static VehicleDB vehicles;
        
        public static VehicleDB getVehicleDB()
        {
            if (vehicles == null)
                vehicles = new VehicleDB();
            return vehicles;
        }

        public static TransportDB getTransportDB()
        {
            if (transports == null)
                transports = new TransportDB();
            return transports;
        }

        public static StorageDB getStorageDB()
        {
            if (storages == null)
                storages = new StorageDB();
            return storages;
        }

        public static OfficeDB getOfficeDB()
        {
            if (offices == null)
                offices = new OfficeDB();
            return offices;
        }

        public static PackageDB getPackageDB()
        {
            if (packages == null)
                packages = new PackageDB();
            return packages;
        }

        public static PersonDB getPersonDB()
        {
            if (pers == null)
                pers = new PersonDB();
            return pers;
        }
    }
}
